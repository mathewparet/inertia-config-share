# mathewparet/inertia-config-share

Shares selected server side configs with Inertia

# Installation

```sh
composer require mathewparet/inertia-config-share
sail artisan vendor:publish --tag=inertia-config-share
```

Configure `config/share.php` and you are good to go.

