<?php
namespace mathewparet\InertiaConfigAndFlashShare;

use Illuminate\Support\ServiceProvider;
use mathewparet\InertiaConfigAndFlashShare\Middleware\ShareConfigAndFlashWithInertia;

class ShareConfigAndFlashProvider extends ServiceProvider
{
    const CONFIG_FILE = __DIR__.'/config/share.php';

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(self::CONFIG_FILE, 'share');

        $this->registerMiddleware();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->definePublishableComponents();
    }

    private function registerMiddleware()
    {
        $router = $this->app['router'];
        
        $this->app->booted(function () use ($router) {
            $router->pushMiddleWareToGroup('web',ShareConfigAndFlashWithInertia::class);  
        });
    }

    private function definePublishableComponents()
    {
        $this->publishes([
            self::CONFIG_FILE => config_path('share.php')
        ], 'inertia-config-share');
    }
}