<?php
namespace mathewparet\InertiaConfigAndFlashShare\Middleware;

use Closure;
use Inertia\Inertia;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class ShareConfigAndFlashWithInertia
{
    /**
     * Handles the given request and returns the response.
     *
     * @param Request $request The request object.
     * @param Closure $next The next closure.
     * @param mixed ...$guards The guards.
     * @throws Some_Exception_Class Description of exception.
     * @return mixed The response.
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        Inertia::share('config', fn() => $this->getExportableConfigurations());

        Inertia::share('flash', fn() => $this->exportFlashMessages($request));

        return $next($request);
    }

    /**
     * Export flash messages from the given request.
     *
     * @param Request $request The request object.
     * @return array An array of flash messages.
     */
    private function exportFlashMessages(Request $request)
    {
        $export = [];

        foreach(config('share.flash') as $flash)
        {
            $export[$flash] = fn() => $request->session()->get($flash);
        }

        return $export;
    }

    /**
     * Retrieves the exportable configurations from the 'share.configs' array and returns them as an array.
     *
     * @return array The exportable configurations.
     */
    private function getExportableConfigurations()
    {
        $export = [];

        foreach (config('share.configs') as $config) {
            $export[$config] = config($config);
        }

        return Arr::undot($export);
    }
}